//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main()     
{
    int a1, a2, sum;
    printf("Enter two numbers = ");
    scanf("%d %d", &a1, &a2);
     sum = a1 + a2;      
    printf("%d + %d = %d", a1, a2, sum);
      return 0;
}
